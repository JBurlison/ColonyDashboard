﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using UnityEngine;

namespace Pandaros.ColonyDashboard.Shared
{
    public class ColonyLogger
    {
        public enum LogLevel : int
        {
            Debug = 0,
            Information = 1,
            Warning = 2,
            Error = 3
        }


        /// <summary>
        /// Actions that apply to system files.
        /// </summary>
        public enum FileAction : int
        {
            /// <summary>
            /// A command to move a file.
            /// </summary>
            Move = 0,

            /// <summary>
            /// A command to delete a file.
            /// </summary>
            Delete = 1
        }

        public class LoggerOptions
        {
            public LogLevel Level { get; set; }
            public Exception Error { get; set; }
            public string Message { get; set; } = string.Empty;
        }


        /// <summary>
        /// Look for file in the format xxxx.99999.log
        /// The number is extracted from both filenames and this number is used for the comparison
        /// </summary>
        public class LogFileSorter : Comparer<string>
        {
            public override int Compare(string x, string y)
            {
                if (string.IsNullOrEmpty(x))
                {
                    if (string.IsNullOrEmpty(y))
                    {
                        return 0;
                    }
                    else
                    {
                        return -1;
                    }
                }
                else
                {
                    if (string.IsNullOrEmpty(y))
                    {
                        return 1;
                    }
                    else
                    {
                        int xval = GetNumFromFilename(x);
                        int yval = GetNumFromFilename(y);

                        return xval.CompareTo(yval);
                    }
                }
            }
        }

        private string _logFile = string.Empty;
        private DirectoryInfo _logfileDir;
        private const int LOGGER_TRY = 1000; 
        private const string DOT_LOG = ".log";
        private const string ONE_DOT_LOG = ".1.log";
        private const string LOG_FORMAT = "[{0}] {1}: ";
        private const string LINEFEED_RETURN = "\r\n";
        private const string DOT_STAR_DOT_LOG = ".*.log";
        private static char[] dot = new char[] { '.' };
        public  LogLevel MinimumLogLevel;

        private  Queue<LoggerOptions> _logQueue = new Queue<LoggerOptions>();
        public  string AppName { get; set; }
        public  bool Running { get; set; }

        public static bool ISMOD { get; private set; }

        public ColonyLogger(string appName, DirectoryInfo logDir, LogLevel minLogLevel, bool isMod)
        {
            _logfileDir = logDir;
            AppName = appName;
            ISMOD = isMod;

            if (!_logfileDir.Exists)
                _logfileDir.Create();

            _logFile = Path.Combine(_logfileDir.FullName, AppName + DOT_LOG);
            MinimumLogLevel = minLogLevel;
            Running = true;

            Thread t = new Thread(() => LogMessages());
            t.IsBackground = true;
            t.Start();

            LogDebug("Logger initialized");
        }

        #region Log Rotation

        private  void LogMessages()
        {
            while (Running)
            {
                try
                {
                    var msg = _logQueue.Dequeue();

                    if (msg == null)
                        continue;

                    DateTime dt = DateTime.Now;
                    string date = dt.ToString();

                    string lvlString = msg.Level.ToString();

                    string logmsg = string.Format(LOG_FORMAT, AppName, lvlString);

                    if (msg.Message != null)
                        logmsg += msg.Message;

                    if (msg.Error != null)
                    {
                        logmsg += LINEFEED_RETURN + msg.Error.StackTrace.ToString().Trim();
                    }

                    if (ISMOD)
                        switch (msg.Level)
                        {
                            case LogLevel.Error:

                                if (msg.Error != null)
                                    ServerLog.LogAsyncExceptionMessage(new Pipliz.LogExceptionMessage(logmsg, msg.Error));
                                else
                                    ServerLog.LogAsyncMessage(new Pipliz.LogMessage(logmsg, LogType.Error));
                                break;

                            case LogLevel.Warning:
                                ServerLog.LogAsyncMessage(new Pipliz.LogMessage(logmsg, LogType.Warning));
                                break;

                            default:
                                ServerLog.LogAsyncMessage(new Pipliz.LogMessage(logmsg, LogType.Log));
                                break;
                        }
                        

                    // log to log file
                    try
                    {
                        using (var logFile = File.Open(DefaultLogFileName(), FileMode.Append))
                        using (var sw = new StreamWriter(logFile))
                            sw.WriteLine(logmsg);
                    }
                    catch (Exception ex)
                    {
                        if (string.IsNullOrEmpty(_logFile))
                            Console.WriteLine("_logFile is NULL or empty string. Unable to log.");
                        else
                            Console.WriteLine("Unable to log to directory {0}", _logFile);

                        Console.WriteLine(ex.Message);
                        Console.WriteLine(ex.StackTrace);

                        if (ex.InnerException != null)
                        {
                            Console.WriteLine(ex.InnerException.Message);
                            Console.WriteLine(ex.InnerException.StackTrace);
                        }
                    }
                }
                finally
                {
                    rotateLogs();
                }
            }
        }

        private  void rotateLogs()
        {
            if (!File.Exists(_logFile)) return; // we don't have a current log
            FileInfo fix = new FileInfo(_logFile);
            long fixedSize = fix.Length / 1024L; // was in bytes, we want in kb
            if (fixedSize >= 100)
            {
                string oldFilename = _logfileDir + AppName + ONE_DOT_LOG;
                if (File.Exists(oldFilename))
                { // rotate down old ones
                    _rotateOld(_logfileDir);
                }
                int j = 0;
                while (!loggerCheck(_logFile, oldFilename, FileAction.Move))
                {
                    j++;
                    if (j > LOGGER_TRY) break;
                }
            }
        }

        private  bool loggerCheck(string f, string oldFilename, FileAction action)
        {
            if (File.Exists(f))
            {
                if (action == FileAction.Move)
                {
                    try
                    {
                        if (File.Exists(oldFilename))
                            File.Delete(oldFilename);
                        File.Move(f, oldFilename);
                        return true;
                    }
                    catch { }
                }
                else if (action == FileAction.Delete)
                {
                    try
                    {
                        File.Delete(f);
                        return true;
                    }
                    catch { }
                }
            }
            return false;
        }

        /// <summary>
        /// Return the log number for a file in the format xxxx.9999.log
        /// If there is an error return 0
        /// </summary>
        /// <param name="filename"></param>
        /// <returns></returns>
        public static int GetNumFromFilename(string filename)
        {
            int filenum = 0;
            if (!string.IsNullOrEmpty(filename))
            {
                string[] xtokens = filename.Split(dot);
                int xtokencount = xtokens.GetLength(0);
                if (xtokencount > 2)
                {
                    int xval = 0;
                    if (Int32.TryParse(xtokens[xtokencount - 2], out xval))
                    {
                        filenum = xval;
                    }
                }
            }
            return filenum;
        }

        private  void _rotateOld(DirectoryInfo logFileDir)
        {
            string[] raw = Directory.GetFiles(logFileDir.FullName, AppName + DOT_STAR_DOT_LOG);
            List<string> files = new List<string>();
            files.AddRange(raw);

            Comparer<string> myComparer = new LogFileSorter();
            files.Sort(myComparer);
            files.Reverse();

            foreach (string f in files)
            {
                int logfnum = GetNumFromFilename(f);
                if (logfnum > 0)
                {
                    string newname = string.Format("{0}\\{1}.{2}.log", _logfileDir, AppName, logfnum + 1);

                    if (logfnum >= 10)
                    {
                        int j = 0;
                        while (!loggerCheck(f, string.Empty, FileAction.Delete))
                        {
                            j++;
                            if (j > LOGGER_TRY) break;
                        }
                    }
                    else
                    {
                        int j = 0;
                        while (!loggerCheck(f, newname, FileAction.Move))
                        {
                            j++;
                            if (j > LOGGER_TRY) break;
                        }
                    }
                }
            }
        }

        #endregion Log Rotation

        public  bool Log(string LogEntry, LoggerOptions options)
        {
            return Log(options.Level, options.Error, LogEntry);
        }

        public  bool LogInformation(string LogEntry)
        {
            return Log(LogLevel.Information, null, LogEntry);
        }

        public  bool LogInformation(string LogEntry, params object[] args)
        {
            return Log(LogLevel.Information, null, string.Format(LogEntry, args));
        }

        public  bool LogInformation(string LogEntry, LoggerOptions options)
        {
            options.Level = LogLevel.Information;
            return Log(LogEntry, options);
        }

        public  bool LogWarning(string LogEntry)
        {
            return Log(LogLevel.Warning, null, LogEntry);
        }

        public  bool LogWarning(string LogEntry, params object[] args)
        {
            return Log(LogLevel.Warning, null, string.Format(LogEntry, args));
        }

        public  bool LogWarning(string LogEntry, LoggerOptions options)
        {
            options.Level = LogLevel.Warning;
            return Log(LogEntry, options);
        }

        public  bool LogWarning(Exception ex)
        {
            return Log(LogLevel.Warning, ex, ex.Message);
        }

        public  bool LogDebug(string sLogDebug)
        {
            return Log(LogLevel.Debug, null, sLogDebug);
        }

        public  bool LogDebug(string LogEntry, params object[] args)
        {
            return Log(LogLevel.Debug, null, string.Format(LogEntry, args));
        }

        public  bool LogDebug(string sLogEntry, LoggerOptions options)
        {
            options.Level = LogLevel.Debug;
            return Log(sLogEntry, options);
        }

        public  bool LogError(string LogEntry)
        {
            return Log(LogLevel.Error, null, LogEntry);
        }

        public  bool LogError(string LogEntry, params object[] args)
        {
            return Log(LogLevel.Error, null, string.Format(LogEntry, args));
        }

        public  bool LogError(string LogEntry, LoggerOptions options)
        {
            options.Level = LogLevel.Error;
            return Log(LogEntry, options);
        }

		public  bool LogError(Exception e)
        {
            return Log(LogLevel.Error, e, e.Message);
        }

        public  bool LogError(Exception e, string LogEntry)
        {
            Log(LogLevel.Error, null, LogEntry);
            return Log(LogLevel.Error, e, e.Message);
        }

        public  bool LogError(Exception e, string LogEntry, params object[] args)
        {
            Log(LogLevel.Error, null, string.Format(LogEntry, args));
            return Log(LogLevel.Error, e, e.Message);
        }

        private  bool Log(LogLevel level, Exception e, string LogEntry)
        {
            if (MinimumLogLevel > level)
                return true;

            _logQueue.Enqueue(new LoggerOptions()
            {
                Level = level,
                Error = e,
                Message = LogEntry
            });

            return true;
        }

        // get the default log file name
        private  string DefaultLogFileName()
        {
            try
            {
                if (string.IsNullOrEmpty(_logFile))
                    return null;

                if (File.Exists(_logFile))
                    return _logFile; // the file exists, return it.
                else // we must creat the log file first.
                {
                    FileStream fs = new FileStream(_logFile, FileMode.OpenOrCreate, FileAccess.ReadWrite);
                    fs.Dispose();
                }

                return _logFile;
            }
            catch (Exception e)
            {
                Console.WriteLine("Failed to create text log file. " + e.Message);
                return null;
            }
        }
    } // end Logger
}
