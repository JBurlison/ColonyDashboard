﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Pandaros.ColonyDashboard.Shared
{
    public class DBInterface
    {
        public string DBFilelocation
        {
            get
            {
                return ConfiguraitonFile.GetorDefault("DBFilelocation", @"C:\Program Files (x86)\Steam\steamapps\common\Colony Survival\gamedata\ColonyDashboard.db");
            }
        }

        public ConfiguraitonFile ConfiguraitonFile { get; private set; }

        public DBInterface(ConfiguraitonFile configuraiton)
        {
            ConfiguraitonFile = configuraiton;
        }

        
    }
}
