﻿using Pipliz.JSON;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace Pandaros.ColonyDashboard.Shared
{
    public class ConfiguraitonFile
    {
        private FileInfo _settingFile;

        private JSONNode _rootSettings;

        public ConfiguraitonFile(FileInfo configFile)
        {
            _settingFile = configFile;

            if (_settingFile.Exists && JSON.Deserialize(_settingFile.FullName, out var config))
                _rootSettings = config;
            else
                _rootSettings = new JSONNode();
        }

        public void Save()
        {
            JSON.Serialize(_settingFile.FullName, _rootSettings);
        }

        public T GetorDefault<T>(string key, T defaultVal)
        {
            if (!_rootSettings.HasChild(key))
                SetValue(key, defaultVal);

            return _rootSettings.GetAs<T>(key);
        }

        public void SetValue<T>(string key, T val)
        {
            _rootSettings.SetAs(key, val);
            Save();
        }
    }
}
