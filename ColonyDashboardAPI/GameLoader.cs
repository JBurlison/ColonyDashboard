﻿using Pandaros.ColonyDashboard.Shared;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace Pandaros.ColonyDashboard.API
{
    [ModLoader.ModManager]
    public static class GameLoader
    {
        public static ColonyLogger Logger { get; private set; } 
        public static ConfiguraitonFile APIConfiguration { get; private set; }
        
        public static string MOD_FOLDER = @"gamedata/mods/Pandaros/ColonyDashboard.API";
        public static string MODS_FOLDER = @"";
        public static string GAMEDATA_FOLDER = @"";
        public static string GAME_ROOT = @"";

        public const string NAMESPACE = "Pandaros.ColonyDashboard.API";

        public static readonly Version MOD_VER = new Version(0, 0, 1, 0);

        public static bool RUNNING { get; private set; }
        public static bool WorldLoaded { get; private set; }

        [ModLoader.ModCallback(ModLoader.EModCallbackType.AfterWorldLoad, GameLoader.NAMESPACE + ".AfterWorldLoad")]
        public static void AfterWorldLoad()
        {
            WorldLoaded = true;
            Logger.LogInformation("World load detected. Starting monitor...");
        }

        [ModLoader.ModCallback(ModLoader.EModCallbackType.OnAssemblyLoaded, NAMESPACE + ".OnAssemblyLoaded")]
        public static void OnAssemblyLoaded(string path)
        {
            GAME_ROOT = path.Substring(0, path.IndexOf("gamedata")) + "/";
            GAMEDATA_FOLDER = path.Substring(0, path.IndexOf("gamedata") + "gamedata".Length) + "/";
            MODS_FOLDER = GAMEDATA_FOLDER + "/mods/";
            MOD_FOLDER = Path.GetDirectoryName(path);

#if DEBUG
            Logger = new ColonyLogger("ColonyDashboardApi", new DirectoryInfo($"{GAME_ROOT}logs/{NAMESPACE}/"), ColonyLogger.LogLevel.Debug, true);
#else
            Logger = new ColonyLogger("ColonyDashboardApi", new DirectoryInfo($"{GAME_ROOT}logs/{NAMESPACE}/"), ColonyLogger.LogLevel.Information, true);
#endif
            APIConfiguration = new ConfiguraitonFile(new FileInfo($"{GAMEDATA_FOLDER}{NAMESPACE}.json"));
        }


    }
}
