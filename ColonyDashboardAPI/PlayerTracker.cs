﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Pandaros.ColonyDashboard.API
{
    [ModLoader.ModManager]
    public static class PlayerTracker
    {
        public static event EventHandler PreformSnapshot;



        [ModLoader.ModCallback(ModLoader.EModCallbackType.OnPlayerConnectedLate, GameLoader.NAMESPACE + ".PlayerTracker.OnPlayerConnectedLate")]
        public static void OnPlayerConnectedLate(Players.Player p)
        {

        }

        [ModLoader.ModCallback(ModLoader.EModCallbackType.OnPlayerDisconnected, GameLoader.NAMESPACE + ".PlayerTracker.OnPlayerDisconnected")]
        public static void OnPlayerDisconnected(Players.Player p)
        {

        }
    }
}
